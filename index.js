const express = require('express');
const app = express();
const productRouter = require('./router/productRouter');
const customerRouter = require ('./router/customerRouter')

app.use(express.json());


app.use(productRouter)
app.use(customerRouter)


app.listen(3000, () => {
    console.log(`Server started on 3000`);
});

